const path = require('path');

module.exports.validate = (req, res, next) => {
    const { filename, content } = req.body;
    const regex = new RegExp('(.*?).(txt|json|yaml|xml|js)$');

    if (!filename) {
        console.log(`Please specify 'filename' parameters`);
        return res.status(400).json({ message: `Please specify 'filename' parameters` });
    }

    if (!content) {
        console.log(`Please specify 'content' parameters`);
        return res.status(400).json({ message: `Please specify 'content' parameters` });
    }

    if (!regex.test(filename)) {
        console.log('Format is not correct');
        return res.status(400).json({ message: `Format file is not correct` });
    }

    next();
};
