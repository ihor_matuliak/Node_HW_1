const path = require('path');
const fs = require('fs');

module.exports.createFile = async (req, res) => {
    const { filename, content } = req.body;

    fs.mkdir(path.join('./', 'files'), (err) => {});

    fs.writeFile(path.join(__dirname, '../files', filename), content, (err) => {
        if (err) {
            res.status(500).json({ message: 'Server error' });
            console.log('Server error');
        } else {
            res.status(200).json({ message: 'File created successfully' });
            console.log('File created successfully');
        }
    });
};

module.exports.getFiles = async (req, res) => {
    fs.readdir(path.join(__dirname, '../files'), (err, files) => {
        if (!err) {
            res.status(200).json({
                message: 'Success',
                files,
            });
            console.log('Success', files);
        } else {
            res.status(500).json({
                message: 'Server error',
            });
            console.log('Server error');
        }
    });
};

module.exports.getFile = async (req, res) => {
    const filename = req.params.filename;

    fs.readFile(path.join(__dirname, '../files', filename), 'utf-8', (err, data) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({ message: `No file with '${filename}' filename found` });
            }

            res.status(500).json({
                message: 'Server error',
            });
        } else {
            const extension = path.extname(filename);

            res.status(200).json({
                message: 'Success',
                name: filename,
                content: data,
                extension: extension,
            });
        }
    });
};

module.exports.delete = (req, res) => {
    const { filename } = req.params;

    fs.unlink(path.join(__dirname, '../files', filename), (err) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({
                    message: `No file with ${filename} filename found`,
                });
            }
            return res.status(500).json({ message: 'Server error' });
        }
        console.log('File delete');
        return res.status(200).json({ message: `The ${filename} was deleted` });
    });
};
