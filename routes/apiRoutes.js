const express = require('express');
const router = express.Router();
const fileController = require('../controllers/fileController');
const validateFile = require('../middlewares/validateFile');

router.post('/', validateFile.validate, fileController.createFile);
router.get('/', fileController.getFiles);
router.get('/:filename', fileController.getFile);
router.delete('/:filename', fileController.delete);

module.exports = router;
